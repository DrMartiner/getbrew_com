import os
from pathlib import Path
from socket import gethostbyname
from uuid import uuid4

import dj_database_url
import environ
import validators

root = environ.Path(__file__, "../..")

os.sys.path.insert(0, root())
os.sys.path.insert(0, os.path.join(root(), "apps"))

env = environ.Env(
    ROLE=(str, "prod"),
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, []),
    SECRET_KEY=(str, "$lf#k#3)j+qf+iq+#l!2gze$7rq0!=91_o#aoh&%!4=-z6(pgp"),
    DB_DSN=(str, "postgres://user:pass@pg/main"),
    BASE_URL=(str, "http://localhost:8000"),
    REDIS_DSN=(str, "redis://redis:6379/0"),
    CELERY_ALWAYS_EAGER=(bool, False),
    NOTIFICATION_API_URL=(str, "https://notificaotr:8080/api/v1"),
)

BASE_DIR = Path(__file__).resolve().parent.parent

ROLE = env("ROLE")
DEBUG = env("DEBUG")

SECRET_KEY = env("SECRET_KEY")

DB_DSN = env("DB_DSN")
DATABASES = {"default": dj_database_url.config(default=DB_DSN)}
DATABASES["default"]["TEST"] = {"NAME": f"test_main_{uuid4().hex[:6]}"}
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

ROOT_URLCONF = "project.urls"
WSGI_APPLICATION = "project.wsgi.application"

ALLOWED_HOSTS = env("ALLOWED_HOSTS")
allowed_hosts = set()
for host in ALLOWED_HOSTS:
    if not validators.domain(host):
        continue

    try:
        ip: str = gethostbyname(host)
        allowed_hosts.add(ip)
    except Exception as e:
        ...

ALLOWED_HOSTS += list(allowed_hosts)

INSTALLED_APPS = [
    "apps.core.apps.CoreConfig",
    "corsheaders",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_celery_beat",
    "django_celery_results",
    "drf_yasg",
    "rest_framework",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [root("project/templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]


USE_TZ = True
TIME_ZONE = "UTC"

LANGUAGE_CODE = "en"
USE_I18N = True
USE_L10N = True

MEDIA_URL = "/media/"
MEDIA_ROOT = root("media")

STATIC_URL = "/static/"
STATIC_ROOT = root("static")
STATICFILES_DIRS = [root("project/static")]

APPEND_SLASH = True
BASE_URL = env("BASE_URL")
LOGIN_URL = "/admin/login/"

REST_FRAMEWORK = {
    "PAGE_SIZE": 20,
    "UPLOADED_FILES_USE_URL": env("ROLE") != "test",
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "TEST_REQUEST_DEFAULT_FORMAT": "json",
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
    "DEFAULT_PERMISSION_CLASSES": [],
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "root": {"level": "WARNING", "handlers": ["console"]},
    "formatters": {
        "verbose": {"format": "%(levelname)s  %(asctime)s  %(module)s: %(message)s"},
    },
    "handlers": {"console": {"level": "DEBUG", "class": "logging.StreamHandler", "formatter": "verbose"}},
    "loggers": {
        "notificator": {"level": "INFO", "handlers": ["console"], "propagate": False},
        "celery": {"level": "INFO", "handlers": ["console"], "propagate": False},
        "celery.tasks": {"level": "INFO", "handlers": ["console"], "propagate": False},
        "django.server": {"level": "INFO", "handlers": ["console"], "propagate": False},
        "django.request": {"level": "INFO", "handlers": ["console"], "propagate": False},
        "django.db.backends": {"level": "ERROR", "handlers": ["console"], "propagate": False},
    },
}

REDIS_DSN = env("REDIS_DSN")
BROKER_URL = REDIS_DSN
CELERY_BROKER_URL = REDIS_DSN
CELERY_RESULT_BACKEND = REDIS_DSN
CELERY_ENABLE_UTC = False
CELERY_TIMEZONE = TIME_ZONE
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_ALWAYS_EAGER = env("CELERY_ALWAYS_EAGER")

NOTIFICATION_API_URL = env("NOTIFICATION_API_URL")

if ROLE == "test":
    PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

    class DisableMigrations(object):
        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return None

    MIGRATION_MODULES = DisableMigrations()
