from django.urls import include, path

urlpatterns = [
    path("core/", include(("apps.api.core.urls", "apps.api"), namespace="core")),
    path("docs/", include(("apps.api.docs.urls", "apps.api"), namespace="docs")),
]
