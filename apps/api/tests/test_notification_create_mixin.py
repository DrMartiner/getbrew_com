from typing import Tuple

import mock
from django_dynamic_fixture import G
from rest_framework.serializers import ModelSerializer

from apps.api.base_test import BaseApiTest
from apps.api.views import NotificationCreateMixin
from apps.core.models import Company


class View(NotificationCreateMixin):
    ...


class SerializerClass(ModelSerializer):
    class Meta:
        model = Company
        fields = ["employees_min"]


class NotificationCreateMixinTest(BaseApiTest):
    view: View
    instance: Company

    def setUp(self) -> None:
        super(NotificationCreateMixinTest, self).setUp()

        self.view = View()
        self.instance: Company = G(Company)
        self.serializer = SerializerClass(instance=self.instance, data={"employees_min": 1})
        self.serializer.is_valid(raise_exception=True)

    def test_succeed(self) -> None:
        with mock.patch("apps.core.tasks.call_api.apply_async") as m:  # type: mock.MagicMock
            self.view.perform_create(self.serializer)

            self.assertEqual(m.call_count, 1)

            arguments: Tuple[str, str, dict] = m.call_args.args[0]
            self.assertEqual(len(arguments), 3)
            self.assertEqual(arguments[0], Company.__name__.lower())
            self.assertEqual(arguments[1], "created")
            self.assertDictEqual(arguments[2], self.serializer.data)
