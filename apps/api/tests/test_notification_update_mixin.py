from typing import Any, Tuple

import mock
from django_dynamic_fixture import G
from rest_framework.serializers import ModelSerializer

from apps.api.base_test import BaseApiTest
from apps.api.views import NotificationUpdateMixin
from apps.core.models import CRAWLING_STATUSES, Company, ContentItem


class NotificationUpdateMixinWatchFieldsTest(BaseApiTest):
    def test_succeed(self) -> None:
        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"field1": None, "field2": ["value1", "value2"]}

        view = View()

        self.assertDictEqual(view.watch_fields, View.WATCH_FIELDS)

    def test_has_not_attribute(self) -> None:
        class View(NotificationUpdateMixin):
            ...

        view = View()

        with self.assertRaises(AttributeError):
            watch_fields = view.watch_fields  # noqa


class NotificationUpdateMixinExtractOldValuesTest(BaseApiTest):
    def test_simple_values(self):
        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"employees_min": None, "employees_max": [1, 2]}

        instance: Company = G(Company, employees_min=1, employees_max=2)
        view = View()

        old_values = view.extract_old_values(instance)
        self.assertDictEqual(old_values, {"employees_min": 1, "employees_max": 2})

    def test_fk_value(self):
        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"company": None}

        instance: ContentItem = G(ContentItem)
        view = View()

        old_values = view.extract_old_values(instance)
        self.assertDictEqual(old_values, {"company": instance.company})


class NotificationUpdateMixinExtractChangesTest(BaseApiTest):
    def test_any_value_int(self):
        NEW_VALUE = 2

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"employees_min": None}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = Company
                fields = ["employees_min"]

        instance: Company = G(Company, employees_min=1)
        serializer = SerializerClass(instance=instance, data={"employees_min": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        changes = view.extract_changes(serializer, {"employees_min": instance.employees_min})
        self.assertHasFieldInChanges(changes, "employees_min", instance.employees_min, NEW_VALUE)

    def test_any_value_bool(self):
        NEW_VALUE = False

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"is_deleted": None}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = Company
                fields = ["is_deleted"]

        instance: Company = G(Company, is_deleted=True)
        serializer = SerializerClass(instance=instance, data={"is_deleted": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        changes = view.extract_changes(serializer, {"is_deleted": instance.is_deleted})
        self.assertHasFieldInChanges(changes, "is_deleted", True, NEW_VALUE)

    def test_any_value_pk(self):
        new_company: Company = G(Company)
        NEW_VALUE = new_company.pk

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"company": None}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = ContentItem
                fields = ["company"]

        old_company: Company = G(Company)
        instance: ContentItem = G(ContentItem, company=old_company)
        serializer = SerializerClass(instance=instance, data={"company": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        changes = view.extract_changes(serializer, {"company": old_company})
        self.assertHasFieldInChanges(changes, "company", instance.company.pk, NEW_VALUE)

    def test_target_value_in(self):
        NEW_VALUE = CRAWLING_STATUSES.ERROR_REQUESTING_LINK

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"crawling_status": [CRAWLING_STATUSES.ERROR_REQUESTING_LINK, CRAWLING_STATUSES.UNCRAWLABLE]}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = Company
                fields = ["crawling_status"]

        instance: Company = G(Company, crawling_status=CRAWLING_STATUSES.CRAWLING_FAILED)
        serializer = SerializerClass(instance=instance, data={"crawling_status": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        changes = view.extract_changes(serializer, {"crawling_status": instance.crawling_status})
        self.assertHasFieldInChanges(changes, "crawling_status", instance.crawling_status, NEW_VALUE)

    def test_target_value_not_in(self):
        NEW_VALUE = CRAWLING_STATUSES.ERROR_REQUESTING_LINK

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"crawling_status": [CRAWLING_STATUSES.UNCRAWLABLE]}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = Company
                fields = ["crawling_status"]

        instance: Company = G(Company, crawling_status=CRAWLING_STATUSES.CRAWLING_FAILED)
        serializer = SerializerClass(instance=instance, data={"crawling_status": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        changes = view.extract_changes(serializer, {"crawling_status": instance.crawling_status})
        self.assertHasNotFieldInChanges(changes, "crawling_status")

    def assertHasFieldInChanges(self, changes: Any, field_name: str, old_value: Any, new_value: Any) -> None:
        self.assertIsInstance(changes, dict)

        self.assertIn(field_name, changes)
        self.assertIsInstance(changes[field_name], dict)

        self.assertIn("old", changes[field_name])
        self.assertEqual(changes[field_name]["old"], old_value)

        self.assertIn("new", changes[field_name])
        self.assertEqual(changes[field_name]["new"], new_value)

    def assertHasNotFieldInChanges(self, changes: Any, field_name: str) -> None:
        self.assertIsInstance(changes, dict)

        self.assertNotIn(field_name, changes)


class NotificationUpdateMixinPerformUpdateTest(BaseApiTest):
    def test_has_changes(self):
        NEW_VALUE = True

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"is_deleted": None}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = Company
                fields = ["is_deleted"]

        instance: Company = G(Company, is_deleted=False)
        serializer = SerializerClass(instance=instance, data={"is_deleted": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        with mock.patch("apps.core.tasks.call_api.apply_async") as m:  # type: mock.MagicMock
            view.perform_update(serializer)

            self.assertEqual(m.call_count, 1)

            arguments: Tuple[str, str, dict] = m.call_args.args[0]
            self.assertEqual(len(arguments), 3)
            self.assertEqual(arguments[0], Company.__name__.lower())
            self.assertEqual(arguments[1], "updated")
            self.assertDictEqual(
                arguments[2], {"id": instance.pk, "changes": {"is_deleted": {"old": False, "new": NEW_VALUE}}}
            )

    def test_has_not_changes(self):
        NEW_VALUE = True

        class View(NotificationUpdateMixin):
            WATCH_FIELDS = {"is_deleted": None}

        class SerializerClass(ModelSerializer):
            class Meta:
                model = Company
                fields = ["is_deleted"]

        instance: Company = G(Company, is_deleted=NEW_VALUE)
        serializer = SerializerClass(instance=instance, data={"is_deleted": NEW_VALUE})
        serializer.is_valid(raise_exception=True)
        view = View()

        with mock.patch("apps.core.tasks.call_api.apply_async") as m:  # type: mock.MagicMock
            view.perform_update(serializer)

            self.assertEqual(m.call_count, 0)
