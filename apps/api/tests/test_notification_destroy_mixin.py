from typing import Tuple

import mock
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.views import NotificationDestroyMixin
from apps.core.models import Company


class View(NotificationDestroyMixin):
    ...


class NotificationDeleteMixinTest(BaseApiTest):
    view: View
    instance: Company

    def setUp(self) -> None:
        super(NotificationDeleteMixinTest, self).setUp()

        self.view = View()
        self.instance: Company = G(Company)

    def test_succeed(self) -> None:
        instance_id = self.instance.pk

        with mock.patch("apps.core.tasks.call_api.apply_async") as m:  # type: mock.MagicMock
            self.view.perform_destroy(self.instance)

            self.assertEqual(m.call_count, 1)

            arguments: Tuple[str, str, dict] = m.call_args.args[0]
            self.assertEqual(len(arguments), 3)
            self.assertEqual(arguments[0], Company.__name__.lower())
            self.assertEqual(arguments[1], "destroyed")
            self.assertDictEqual(arguments[2], {"id": instance_id})
