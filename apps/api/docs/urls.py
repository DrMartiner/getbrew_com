from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    url(r"^swagger(?P<format>\.json|\.yaml)$", views.schema_view.without_ui(cache_timeout=0), name="schema-json"),
    path("swagger/", views.schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", views.schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
