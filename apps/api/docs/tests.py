from django.urls import reverse

from apps.api.base_test import BaseApiTest


class DocsTest(BaseApiTest):
    url: str

    def setUp(self) -> None:
        super(DocsTest, self).setUp()

        self.url = reverse("api_v1:docs:schema-swagger-ui")

    def test_docs(self):
        response = self.client.get(self.url, {"format": "openapi"})
        self.assertEqual(response.status_code, 200)
