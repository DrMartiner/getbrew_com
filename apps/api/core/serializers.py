from rest_framework import serializers

from apps.core import models

__all__ = [
    "EventSerializer",
    "WebinarSerializer",
    "CompanySerializer",
    "ContentItemSerializer",
    "CompanyForEventSerializer",
    "CompanyForWebinarSerializer",
    "CompanyCompetitorSerializer",
    "CallLogSerializer",
]


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Event
        fields = "__all__"


class WebinarSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Webinar
        fields = "__all__"


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Company
        fields = "__all__"


class ContentItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ContentItem
        fields = "__all__"


class CompanyForEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CompanyForEvent
        fields = "__all__"


class CompanyForWebinarSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CompanyForWebinar
        fields = "__all__"


class CompanyCompetitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CompanyCompetitor
        fields = "__all__"


class CallLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CallLog
        fields = "__all__"
