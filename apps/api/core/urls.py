from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register("event", views.EventModelViewSet, basename="event")
router.register("webinar", views.WebinarModelViewSet, basename="webinar")
router.register("company", views.CompanyModelViewSet, basename="company")
router.register("company-item", views.ContentItemModelViewSet, basename="company-item")
router.register("company-for-event", views.CompanyForEventModelViewSet, basename="company-for-event")
router.register("company-for-webinar", views.CompanyForWebinarModelViewSet, basename="company-for-webinar")
router.register("company-for-competitor", views.CompanyCompetitorModelViewSet, basename="company-for-competitor")
router.register("call-log", views.CallLogReadOnlyModelViewSet, basename="call-log")

urlpatterns = router.urls
