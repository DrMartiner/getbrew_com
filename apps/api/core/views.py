from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from apps.api.views import NotificationCreateMixin, NotificationDestroyMixin, NotificationUpdateMixin
from apps.core import models
from apps.core.models import CRAWLING_STATUSES

from . import serializers

__all__ = [
    "EventModelViewSet",
    "WebinarModelViewSet",
    "CompanyModelViewSet",
    "ContentItemModelViewSet",
    "CompanyForEventModelViewSet",
    "CallLogReadOnlyModelViewSet",
]


class BaseModelViewSet(NotificationCreateMixin, NotificationUpdateMixin, NotificationDestroyMixin, ModelViewSet):
    ...


class EventModelViewSet(BaseModelViewSet):
    queryset = models.Event.objects.all()
    serializer_class = serializers.EventSerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "is_blacklisted": None,
        "crawling_status": [CRAWLING_STATUSES.TEXT_ANALYZED, CRAWLING_STATUSES.TEXT_UPLOADED],
    }


class WebinarModelViewSet(BaseModelViewSet):
    queryset = models.Webinar.objects.all()
    serializer_class = serializers.WebinarSerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "is_blacklisted": None,
        "crawling_status": [CRAWLING_STATUSES.TEXT_ANALYZED, CRAWLING_STATUSES.TEXT_UPLOADED],
    }


class CompanyModelViewSet(BaseModelViewSet):
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "crawling_status": [CRAWLING_STATUSES.TEXT_ANALYZED, CRAWLING_STATUSES.TEXT_UPLOADED],
    }


class ContentItemModelViewSet(BaseModelViewSet):
    queryset = models.ContentItem.objects.all().select_related("company")
    serializer_class = serializers.EventSerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "is_blacklisted": None,
        "crawling_status": [CRAWLING_STATUSES.TEXT_ANALYZED, CRAWLING_STATUSES.TEXT_UPLOADED],
    }


class CompanyForEventModelViewSet(BaseModelViewSet):
    queryset = models.CompanyForEvent.objects.all().select_related("event", "company")
    serializer_class = serializers.CompanyForEventSerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "is_blacklisted": None,
        "event": None,
    }


class CompanyForWebinarModelViewSet(BaseModelViewSet):
    queryset = models.CompanyForWebinar.objects.all().select_related("event", "company")
    serializer_class = serializers.CompanyForWebinarSerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "is_blacklisted": None,
        "webinar": None,
    }


class CompanyCompetitorModelViewSet(BaseModelViewSet):
    queryset = models.CompanyCompetitor.objects.all().select_related("event", "competitor")
    serializer_class = serializers.CompanyCompetitorSerializer
    WATCH_FIELDS = {
        "is_deleted": None,
        "company": None,
    }


class CallLogReadOnlyModelViewSet(ReadOnlyModelViewSet):
    queryset = models.CallLog.objects.all()
    serializer_class = serializers.CallLogSerializer
