import logging
from typing import Any

from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, UpdateModelMixin
from rest_framework.serializers import ModelSerializer

from django.db.models import Model

from apps.core.tasks import call_api

__all__ = ["NotificationCreateMixin", "NotificationUpdateMixin", "NotificationDestroyMixin"]

logger = logging.getLogger("django.request")


class NotificationCreateMixin(CreateModelMixin):
    def perform_create(self, serializer: ModelSerializer) -> None:
        super(NotificationCreateMixin, self).perform_create(serializer)

        call_api.apply_async([serializer.instance.__class__.__name__.lower(), "created", serializer.data])


class NotificationUpdateMixin(UpdateModelMixin):
    # WATCH_FIELDS = {
    #     "field1": None,  # For any values
    #     "field2": ["value1", "value2"],  # For expected values
    # }

    def perform_update(self, serializer: ModelSerializer) -> None:
        old_values = self.extract_old_values(serializer.instance)

        super(NotificationUpdateMixin, self).perform_update(serializer)

        changes = self.extract_changes(serializer, old_values)
        if changes:
            entity = serializer.instance.__class__.__name__.lower()
            call_api.apply_async([entity, "updated", {"id": serializer.instance.pk, "changes": changes}])

    @property
    def watch_fields(self) -> dict:
        if not hasattr(self, "WATCH_FIELDS"):
            raise AttributeError(f"Class {self.__class__.__name__} has not WATCH_FIELDS. See NotificationUpdateMixin")

        return self.WATCH_FIELDS

    def extract_old_values(self, instance: Model) -> dict:
        entity: str = instance.__class__.__name__.lower()

        old_values = {}
        for field_name in self.watch_fields:
            if not hasattr(instance, field_name):
                logger.warning(f"Model {entity} has not field '{field_name}' which mentioned in WATCH_FIELDS")
                continue

            old_values[field_name] = getattr(instance, field_name)

        return old_values

    def extract_changes(self, serializer: ModelSerializer, old_values: dict) -> dict:
        changes = {}

        for field_name in self.watch_fields:
            if field_name not in serializer.initial_data:
                continue  # This field was not sent in request's data

            old_value: Any = old_values[field_name]
            new_value: Any = serializer.initial_data[field_name]

            if isinstance(old_value, Model):
                if old_value.pk != new_value:
                    changes[field_name] = {"old": old_value.pk, "new": new_value}
            else:
                if self.watch_fields[field_name] is None:
                    if old_value != new_value:
                        changes[field_name] = {"old": old_value, "new": new_value}
                else:
                    if new_value in self.watch_fields[field_name]:
                        changes[field_name] = {"old": old_value, "new": new_value}

        return changes


class NotificationDestroyMixin(DestroyModelMixin):
    def perform_destroy(self, instance: Model) -> None:
        instance_id = instance.pk

        super(NotificationDestroyMixin, self).perform_destroy(instance)

        call_api.apply_async([instance.__class__.__name__.lower(), "destroyed", {"id": instance_id}])
