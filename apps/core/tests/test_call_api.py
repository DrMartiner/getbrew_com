from parameterized import parameterized
from requests_mock import Mocker

from apps.common.base_test import BaseTest
from apps.core.models import CallLog
from apps.core.tasks import _call_api
from project import settings


class CallApiTest(BaseTest):
    ENTITY = "company"
    ACTION = CallLog.ACTION.CREATED
    DATA = {"id": 1}
    MOCKED_URL: str

    def setUp(self) -> None:
        super(CallApiTest, self).setUp()

        self.MOCKED_URL = f"{settings.NOTIFICATION_API_URL}/{self.ENTITY}"

    def test_succeed(self):
        STATUS_CODE = 201

        with Mocker() as m:  # type: Mocker
            m.post(self.MOCKED_URL, json={}, status_code=STATUS_CODE)
            _call_api(self.ENTITY, self.ACTION, self.DATA)

    @parameterized.expand([(500 + i,) for i in range(4)])
    def test_failed(self, status_code: int):
        with Mocker() as m:  # type: Mocker
            m.post(self.MOCKED_URL, json={}, status_code=status_code)
            _call_api(self.ENTITY, self.ACTION, self.DATA)

        count: int = CallLog.objects.count()
        self.assertEqual(count, 0)
