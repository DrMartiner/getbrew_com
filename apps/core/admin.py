from jsoneditor.forms import JSONEditor

from django.contrib import admin
from django.db.models import JSONField

from . import models


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "crawling_status",
        "start_date",
        "end_date",
        "location",
        "last_crawled",
        "is_deleted",
        "is_blacklisted",
    ]
    list_filter = ["start_date", "end_date", "crawling_status", "last_crawled", "is_deleted", "is_blacklisted"]
    search_fields = ["name", "description", "location"]


@admin.register(models.Webinar)
class WebinarAdmin(admin.ModelAdmin):
    list_display = ["name", "start_date", "language", "last_crawled", "is_deleted", "is_blacklisted"]
    list_filter = ["start_date", "last_crawled", "is_deleted", "is_blacklisted", "crawling_status"]
    search_fields = ["name", "language", "description"]


@admin.register(models.Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ["name", "employees_min", "employees_max"]
    list_filter = ["last_crawled", "is_deleted", "is_blacklisted", "crawling_status"]
    search_fields = ["name"]


@admin.register(models.ContentItem)
class ContentItemAdmin(admin.ModelAdmin):
    list_display = ["name", "company"]
    list_filter = ["last_crawled", "is_deleted", "is_blacklisted", "crawling_status"]


@admin.register(models.CompanyForEvent)
class CompanyForEventAdmin(admin.ModelAdmin):
    list_display = ["id", "event", "company", "is_deleted", "is_blacklisted"]
    list_filter = ["is_deleted", "is_blacklisted"]


@admin.register(models.CompanyForWebinar)
class CompanyForWebinarAdmin(admin.ModelAdmin):
    list_display = ["id", "webinar", "company", "is_deleted", "is_blacklisted"]
    list_filter = ["is_deleted", "is_blacklisted"]


@admin.register(models.CompanyCompetitor)
class CompanyCompetitorAdmin(admin.ModelAdmin):
    list_display = ["id", "company", "competitor", "is_deleted"]
    list_filter = ["is_deleted"]


@admin.register(models.CallLog)
class CallLogAdmin(admin.ModelAdmin):
    formfield_overrides = {JSONField: {"widget": JSONEditor}}
    radio_fields = {"action": admin.HORIZONTAL}

    list_display = ["id", "entity", "action", "created"]
    list_filter = ["action", "created"]

    def has_add_permission(self, request) -> bool:
        return False

    def has_change_permission(self, request, obj=None) -> bool:
        return False

    def has_delete_permission(self, request, obj=None) -> bool:
        return False
