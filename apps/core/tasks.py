import logging

import requests
from celery import shared_task
from requests.adapters import HTTPAdapter, Retry

from django.conf import settings

from apps.core.models import CallLog

__all__ = ["call_api"]

logger = logging.getLogger("notificator")

session = requests.Session()
retries = Retry(total=3, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])
session.mount("http://", HTTPAdapter(max_retries=retries))  # nosec
session.mount("https://", HTTPAdapter(max_retries=retries))


@shared_task(bind=True)
def call_api(self, entity: str, action: str, data: dict = dict) -> None:
    _call_api(entity, action, data)

    CallLog.objects.create(entity=entity, action=action, data=data)


def _call_api(entity: str, action: str, data: dict = dict) -> None:
    # Make request
    url = f"{settings.NOTIFICATION_API_URL}/{entity}"
    payload = {"action": action, "data": data}
    try:
        response: requests.Response = session.post(url, payload)

        # Logged response
        level = logging.INFO if response.ok else logging.WARNING
        message = f"[{response.status_code}] POST {response.url} data={data}"
        logger.log(level, message)
    except Exception as e:
        logger.exception(f"POST {url} was failure")
