from django.db import models
from django.template.defaultfilters import truncatechars
from django.utils.translation import gettext as _

__all__ = [
    "CRAWLING_STATUSES",
    "Event",
    "Webinar",
    "Company",
    "ContentItem",
    "CompanyForEvent",
    "CompanyForWebinar",
    "CompanyCompetitor",
    "CallLog",
]


class CRAWLING_STATUSES(models.IntegerChoices):
    NOT_CRAWLED = 0, _("Not crawled")
    ERROR_REQUESTING_LINK = 1, _("Error requesting link")
    UPDATING_LINK = 2, _("Updating link")
    MARKED_AS_DUPLICATE = 3, _("Marked as duplicate")
    UPDATED_LINK = 4, _("Updated link")
    CRAWLING = 5, _("Crawling")
    CRAWLING_FAILED = 6, _("Crawling failed")
    RESCHEDULED_LONG_CRAWLING = 7, _("Rescheduled long crawling")
    CRAWLING_TOO_LONG = 8, _("Crawling too long")
    HAS_NO_PAGES = 9, _("Has no pages")
    TEXT_UPLOADED = 10, _("Text uploaded")
    AWAITING_CRAWL = 11, _("Awaiting crawl")
    INDEXED_BY_ELASTIC = 12, _("Indexed by elastic")
    TEXT_ANALYZED = 13, _("Text analyzed")
    DOMAIN_INVALID = 14, _("Domain invalid")
    NO_LINKS_IN_PAGE = 15, _("No links in page")
    UNCRAWLABLE = 16, _("Uncrawlable")


class CrawlableEntity(models.Model):
    link = models.URLField(_("Link"))
    name = models.CharField(_("Name"), max_length=128)
    crawling_status = models.PositiveSmallIntegerField(_("Crawling statuses"), choices=CRAWLING_STATUSES.choices)
    is_deleted = models.BooleanField(_("Is deleted"))
    is_blacklisted = models.BooleanField(_("Is blacklisted"))
    last_crawled = models.DateTimeField(_("Last crawled"))

    def __str__(self) -> str:
        return truncatechars(self.name, 32)

    class Meta:
        abstract = True


class Event(CrawlableEntity):
    start_date = models.DateTimeField(_("Start date"))
    end_date = models.DateTimeField(_("Start date"))
    description = models.TextField(_("Description"))
    location = models.CharField(_("Location"), max_length=256)

    class Meta:
        app_label = "core"


class Webinar(CrawlableEntity):
    start_date = models.DateTimeField(_("Start date"))
    description = models.TextField(_("Description"))
    language = models.CharField(_("Language"), max_length=4)

    class Meta:
        app_label = "core"


class Company(CrawlableEntity):
    employees_min = models.IntegerField(_("Employees min"))
    employees_max = models.IntegerField(_("Employees max"))

    class Meta:
        app_label = "core"


class ContentItem(CrawlableEntity):
    snippet = models.CharField(_("Snippet"), max_length=512)
    company = models.ForeignKey("Company", models.CASCADE, verbose_name=_("Company"))

    class Meta:
        app_label = "core"


class CompanyForEvent(models.Model):
    event = models.ForeignKey("Event", models.CASCADE, verbose_name=_("Event"))
    company = models.ForeignKey("Company", models.CASCADE, verbose_name=_("Company"))
    is_deleted = models.BooleanField(_("Is deleted"))
    is_blacklisted = models.BooleanField(_("Is blacklisted"))

    class Meta:
        app_label = "core"


class CompanyForWebinar(models.Model):
    webinar = models.ForeignKey("Webinar", models.CASCADE, verbose_name=_("Webinar"))
    company = models.ForeignKey("Company", models.CASCADE, verbose_name=_("Company"))
    is_deleted = models.BooleanField(_("Is deleted"))
    is_blacklisted = models.BooleanField(_("Is blacklisted"))

    class Meta:
        app_label = "core"


class CompanyCompetitor(models.Model):
    company = models.ForeignKey("Company", models.CASCADE, related_name="+", verbose_name=_("Company"))
    competitor = models.ForeignKey("Company", models.CASCADE, related_name="+", verbose_name=_("Competitor"))
    is_deleted = models.BooleanField(_("Is blacklisted"))

    class Meta:
        app_label = "core"


class CallLog(models.Model):
    class ACTION(models.TextChoices):
        CREATED = "created", _("Created")
        UPDATED = "updated", _("Updated")
        DESTROYED = "destroyed", _("Destroyed")

    entity = models.CharField(_("Entity"), max_length=64)
    action = models.CharField(_("Action"), max_length=9, choices=ACTION.choices)
    data = models.JSONField(_("Data"))

    created = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        app_label = "core"
        ordering = ["-created"]
