from typing import Any, Optional

from django.db.models import Model

__all__ = ["get_object_or_none"]


def get_object_or_none(model: Any, *args, **kwargs) -> Optional[Model]:
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return
    except Exception as e:
        raise e
