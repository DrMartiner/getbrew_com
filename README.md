# Summary 
I created a test task for [getbrew.com](https://getbrew.com) company.

# Links
- [Admin](https://localhost:8000/admin)
- [Docs (Redoc)](http://localhost:8000/api/v1/docs/redoc)
- [Docs (Swagger)](http://localhost:8000/api/v1/docs/swagger)
- [Full text of task.docx](./docs/Notifier-Home-Assignment.docx)

# Run
## In docker-compose
- run all services:
```shell
docker-compose -f docker-compose.test.yml up -d
```

- enter to "back" container:
```shell
docker-compose -f .docker/docker-compose.local.yml exec back bash
```

- run in container:
```shell
./manage.py migrate
./manage.py collectstatic --noinput
./manage.py createsuperuser
```

- open [localhost:8000/admin/](http://localhost:8000/admin/) and login

## At local
- run services: `docker-compose -f .docker/docker-compose.local.yml up -d redis pg`

- create virtualenv & install requirements
```shell
pyenv virtualenv 3.9.0 getbrew_com 
pyenv activate getbrew_com 

pip install -U pip poetry
poetry install
```

- set envs
```shell
export DEBUG=on
export ROLE=debug
export SECRET_KEY=1
export DJANGO_SETTINGS_MODULE=project.settings
export ALLOWED_HOSTS=*
export DB_DSN=postgres://user:pass@localhost/main
export REDIS_DSN=redis://localhost:6379/0
export BASE_URL=http://localhost:8000
export CELERY_ALWAYS_EAGER=on
```

- apply migrations: `./manage.py migrate`
- create superuser: `./manage.py createsuperuser`
- run WEB server: `./manage.py runserver`
- open [localhost:8000/admin/](http://localhost:8000/admin/) and login

# Test
- create company
```shell
curl -X POST "http://localhost:8000/api/v1/core/company/" \
    -H  "accept: application/json" \
    -H  "Content-Type: application/json" \
    -d '{"link": "https://example.com", "name": "Name", "crawling_status": 0, "is_deleted": true, "is_blacklisted": true, "last_crawled": "2022-03-09T12:11:34.159Z", "employees_min": 0, "employees_max": 0}'
```

- update (use ID from previous step)
```shell
curl -X PATCH "http://localhost:8000/api/v1/core/company/1/" \
    -H  "accept: application/json" \
    -H  "Content-Type: application/json" \
    -d '{"crawling_status": 13}'
```

- delete
```shell
curl -X DELETE "http://localhost:8000/api/v1/core/company/1/"
```

- open [call logs in admin](http://localhost:8000/admin/core/calllog/) and see logs 

# Contribute to project
## Install pre-commit hooks
```shell
pre-commit install
```

## Before each commit
- to run `make update-isort` for updating iSort's config
- to copy from file `.isort.cfg` from property `known_third_party` to  file `pyproject.toml` to property `known_third_party`
- call linters: `make lint` or just `make`

## Run tests
### Set envs
```shell
export DEBUG=off
export ROLE=test
export SECRET_KEY=1
export DJANGO_SETTINGS_MODULE=project.settings
export ALLOWED_HOSTS=*
export DB_DSN=postgres://user:pass@localhost/main
export REDIS_DSN=redis://localhost:6379/0
export BASE_URL=http://localhost:8000
```